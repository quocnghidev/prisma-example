## RestAPI Prisma, Postgres, Express Example

### Install dependencies

```
npm i
```

### Run postgres with Docker

```
docker-compose up -d
```

### Create sample data

```
npx prisma migrate dev
```

### Run server

```
npm run start:dev
```
