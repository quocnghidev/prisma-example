import { PrismaClient } from '@prisma/client';
import express from 'express';

const prisma = new PrismaClient();
const app = express();

app.use(express.json());
app.use(express.urlencoded({ extended: true }));

//TODO [GET] /users => fetches all users
app.get('/users', async (req, res) => {
  const users = await prisma.user.findMany({ include: { posts: true } });
  res.json(users);
});

// [GET] /feed => fetches all published posts
app.get('/feed', async (req, res) => {
  const posts = await prisma.post.findMany({
    where: { published: true },
    include: { author: true },
  });

  res.json(posts);
});

//TODO [GET /post/:id fetches a specific post by its ID
app.get('/post/:id', async (req, res) => {
  const { id } = req.params;
  const post = await prisma.post.findUnique({
    where: { id: Number(id) },
  });

  res.json(post);
});

//TODO [POST] /user creates a new user
app.post('/user', async (req, res) => {
  const result = await prisma.user.create({
    data: { ...req.body },
  });

  res.json(result);
});

//TODO [POST] /post creates a new post
app.post('/post', async (req, res) => {
  const { title, content, authorEmail } = req.body;
  const result = await prisma.post.create({
    data: {
      title,
      content,
      published: false,
      author: { connect: { email: authorEmail } },
    },
  });

  res.json(result);
});

//TODO [PUT] /post/publish/:id sets the published field of a post to true by its id
app.put('/post/publish/:id', async (req, res) => {
  const { id } = req.params;
  const post = await prisma.post.update({
    where: { id: Number(id) },
    data: { published: true },
  });

  res.json(post);
});

//TODO [DELETE] /post/:id deletes a post by its id
app.delete('/post/:id', async (req, res) => {
  const { id } = req.params;
  const post = await prisma.post.delete({
    where: { id: Number(id) },
  });

  res.json(post);
});

app.listen(3000, () =>
  console.log('REST API server ready at: http://localhost:3000'),
);
